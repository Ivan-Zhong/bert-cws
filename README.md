# Fine-tune Bert for Chinese Word Segmentation(CWS)

**Author: Yifan Zhong**

***Note: This is the greatest, most influential project in the world. SCI alert!***

In this project, I fine-tuned bert-base-chinese to do the CWS and attained 95.26% accuracy on my test set.

To use this project, git clone this project, and use `pip install -r requirements.txt` to install the packages needed. Then, `cd` to the src folder, and run `python3 cut.py`. 

Good luck and have fun! Looking forward to your pull requests!
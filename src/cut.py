from transformers import AutoTokenizer
from transformers import BertModel
from transformers import logging
import json
import torch
import torch.nn as nn
import os

class CWS(nn.Module):
    def __init__(self):
        super().__init__()
        self.layer1 = nn.Linear(768, 1024)
        self.layer2 = nn.Linear(1024, 64)
        self.layer3 = nn.Linear(64, 4)
        self.relu = nn.ReLU()
    
    def forward(self, x):
        out = self.layer1(x)
        out = self.relu(out)
        out = self.layer2(out)
        out = self.relu(out)
        out = self.layer3(out)
        return out

def cut(s, model, bert, tokenizer, device):
    with torch.no_grad():
        tags = ["b", "m", "e", "s"]
        words = list(s)
        inputs = tokenizer(words, is_split_into_words=True, return_tensors='pt').to(device)
        output = bert(inputs["input_ids"])["last_hidden_state"]
        length = output.shape[1]
        output = output[0, 1:length - 1, :]
        output.to(device)
        out = model(output)
        pred = out.argmax(axis = -1)
        pred = pred.squeeze().tolist()
        res = list(map(lambda x: tags[x], pred))

        # output
        length = len(s)
        segs = []
        string = s[0]
        for i in range(1, length):
            if res[i - 1] == 's':
                segs.append(string)
                string = s[i]
            elif res[i - 1] == 'b':
                if res[i] == 'm' or res[i] == 'e':
                    string += s[i]
                else:
                    segs.append(string)
                    string = s[i]
            elif res[i - 1] == 'm':
                if res[i] == 'm' or res[i] == 'e':
                    string += s[i]
                else:
                    segs.append(string)
                    string = s[i]
            else:
                segs.append(string)
                string = s[i]
        segs.append(string)
        return segs

if __name__ == "__main__":

    print("正在加载模型，请稍等......")

    logging.set_verbosity_error()
    tokenizer = AutoTokenizer.from_pretrained("bert-base-chinese")
    bert = BertModel.from_pretrained("bert-base-chinese")
    CWSModel = CWS()
    m_state_dict = torch.load("../model/CWSModel.pt")
    CWSModel.load_state_dict(m_state_dict)

    device = "cuda:0" if torch.cuda.is_available() else "cpu"
    CWSModel.to(device)
    bert.to(device)

    print("模型加载完毕。")

    while True:
        print()
        print("请选择输入输出方式，按q退出：（1）文件；（2）控制台")
        choice = input()
        if choice == 'q':
            print("感谢使用，再见！")
            break
        elif choice == '1':
            infilename = ""
            exit = False
            while True:
                print("请您输入待分词文件的路径，按q返回上一层：")
                infilename = input()
                if infilename == 'q':
                    exit = True
                    break
                if os.path.exists(infilename) == False:
                    print("【错误】此文件不存在。")
                    continue
                break
            if exit:
                continue
            print("请您输入输出文件的路径，按q返回上一层：")
            outfilename = input()
            if outfilename == 'q':
                continue
            with open(outfilename, "w") as outfile:
                with open(infilename, "r") as infile:
                    for line in infile:
                        segs = cut(line, CWSModel, bert, tokenizer, device)
                        outfile.write(segs[0])
                        for i in range(1, len(segs)):
                            outfile.write(f"/{segs[i]}")
                        outfile.write("\n")
        else:
            print("请您输入一句中文，按q返回上一层：")
            s = input()
            if s == 'q':
                continue
            segs = cut(s, CWSModel, bert, tokenizer, device)
            print("分词结果：")
            print(segs[0], end="")
            for i in range(1, len(segs)):
                print(f"/{segs[i]}", end="")
            print()
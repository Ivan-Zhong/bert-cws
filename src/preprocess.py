from transformers import BertTokenizerFast
from sklearn.model_selection import train_test_split
import json

tokenizer = BertTokenizerFast.from_pretrained("bert-base-chinese")

corr = {"b": 0, "m": 1, "e": 2, "s": 3}
data = []

with open("../data/original.txt", "r") as file:
    for line in file:
        sentence, labels = line.strip().split("\t")
        sentence = list(sentence)
        labels = list(map(lambda x: corr[x], list(labels)))
        tokens = tokenizer(sentence, is_split_into_words = True)
        if len(tokens["input_ids"]) > 512:
            continue
        data.append({"tokens": sentence, "labels": labels})

print(f"data length: {len(data)}")

train_validation, test = train_test_split(data, test_size = 0.1, random_state = 1951247)
train, validation = train_test_split(train_validation, test_size = 0.1, random_state = 1951247)
print(f"train length: {len(train)}")
print(f"validation length: {len(validation)}")
print(f"test length: {len(test)}")

def add_id(dataset):
    i = 0
    for data in dataset:
        data["id"] = i
        i += 1

add_id(train)
add_id(validation)
add_id(test)

def serialize(dataset, filename):
    dataset_dict = {"version": "1.0", "data": dataset}
    with open(filename, "w", encoding = "utf8") as file:
        json.dump(dataset_dict, file, ensure_ascii=False)

serialize(train, "../data/train.json")
serialize(validation, "../data/validation.json")
serialize(test, "../data/test.json")
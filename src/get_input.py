import random

with open("../data/test_input.txt", "w") as out:
    with open("../data/original.txt", "r") as infile:
        lines = infile.readlines()
        selected = random.sample(lines, 100)
        for line in selected:
            sen = line.strip().split("\t")[0]
            out.write(sen)
            out.write("\n")